# 🌲 前言
Oracle 学习自然离不开 Google ，你跟我说还有 Baidu ？好吧，谁用谁知道，搜出来的不是广告，就基本都是抄袭复制粘贴！很难找到自己想要找的答案，Google 却可以，但是很多朋友都说无法访问，所以到底如何科学使用 Google ？
# ⭐️ VPN
国内大多数 VPN 都很鸡肋，要么是经常被墙，要么是不稳定，要么是网速差。这里我介绍一个 `VPN` 网站：[cordcloud](https://www.c-cloud.xyz/)。我使用了半年多，可以说是很稳定，没有账户限制，网速很快。是我朋友推荐的，我也推荐给很多朋友，用过的都说好！但是，有部分朋友表示不会使用，所以我打算写一篇来介绍下！

> **<font color='red'>📢  首先申明，此 VPN 需要付费，但是很便宜。纯白嫖的可以不用往下看了！</font> ⭐️ 我推荐这个，我没有任何收益！仅仅推荐而已，方便大家学习，如果有朋友想试一下，我的账号可以借你体验一下。❤️</font>**

**<font color='green'>接下来，教大家如何使用：</font>**

## ① 登录网站

没注册的自己注册下，邮箱即可。

![image.png](https://img-blog.csdnimg.cn/img_convert/700ea0aa66bea7de7763ec1f873a47e8.png)

## ② 商店购买

点击左侧 `交易` 面板，有商店选项，关于收费情况可以自行查看，不做演示，只能说很便宜！

![image.png](https://img-blog.csdnimg.cn/img_convert/04161905373a3c8f3d042ac1e925ed15.png)

我买的是一年的套餐，`1T` 流量：

![image.png](https://img-blog.csdnimg.cn/img_convert/38c5279b1fd23b0f375ac471b281b838.png)

**注意：不购买无法使用！**

## ③ 配置连接
点击首页，滑到底部，可以看到支持 Windows，MACOS，IOS，安卓，甚至路由器也可。

![image.png](https://img-blog.csdnimg.cn/img_convert/a6f7e026dc2f2afa3161554f73bc047a.png)

这里显示如何配置连接的教程，包括你的 `SSR订阅地址`。

这里我演示下 `MACOS` 和 `Windows` 的配置方法：

###  MACOS
根据教程下载 shadowsocks 软件，解压复制到应用程序即可。

![image.png](https://img-blog.csdnimg.cn/img_convert/7a5060ef12b108490cd8cf7fc1cb850f.png)

双击打开软件，在你的 Title 上有一个小飞机的图标，点击它：

![image.png](https://img-blog.csdnimg.cn/img_convert/40a2351b0adba40611fc9189fda9e6bc.png)

点击编辑订阅：

![image.png](https://img-blog.csdnimg.cn/img_convert/2e277f2a6b74fd3e681d69cca5f36abf.png)

新增一个订阅，这里填入你的 SSR订阅地址：

![image.png](https://img-blog.csdnimg.cn/img_convert/4354ce2f0c1190488877c7a0c8768c6b.png)

先点击 `手动更新订阅`，然后勾选 `打开时自动更新订阅`：

![image.png](https://img-blog.csdnimg.cn/img_convert/d122bf11e288b81894f2c642474d92b7.png)

之后就可以使用了，这里有很多的节点，可以根据自己喜好选择：

![image.png](https://img-blog.csdnimg.cn/img_convert/8ebce8bdd59b1df2048c66f6bbda877b.png)

已经可以登录 YouTube 访问关注 Oracle 官方账号，接收最新资讯：

![image.png](https://img-blog.csdnimg.cn/img_convert/f3ef640eda2198d566d8c345b8e878cc.png)


### 💻 Windows

跟 macOS 差不多的配置，同样是下载软件，然后添加订阅地址，选择节点就可以访问：

![image.png](https://img-blog.csdnimg.cn/img_convert/fbeffbdaa2679e86678e6c7dd16efdb0.png)

![image.png](https://img-blog.csdnimg.cn/img_convert/f606e6e94e2302c3e11147d6d41dd444.png)

添加 SSR订阅地址：

![image.png](https://img-blog.csdnimg.cn/img_convert/63f986bcf2f6ac32fbf88243ecfee836.png)

手动更新订阅：

![image.png](https://img-blog.csdnimg.cn/img_convert/807a86d6b075e1916ef848c107923113.png)

更新完之后，这里会出现 cordcloud 列表，代表已经成功。

![image.png](https://img-blog.csdnimg.cn/img_convert/f4f9ce6ece1b24d8b9b10b486caec59e.png)

# ❄️ 写在最后
本文仅分享用于学习使用，如有问题，概不负责。**<font color='red'>本文禁止私下传播！</font>**